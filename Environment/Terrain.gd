extends Spatial

var terrain_z_offset : float = 199
var observed_tile: Spatial  
var tile_to_destroy: Spatial  
var terrain_ref: String = "res://Environment/Terrain_model/GroundHolder.tscn"
var start_z = -82.0

func _ready():
	observed_tile = $GroundHolder

func _process(_delta):
	check_tile_position()

func check_tile_position():
	if observed_tile.transform.origin.z > start_z:
		add_remove_tiles()
	
func add_remove_tiles():
	print("new tiles")
	if tile_to_destroy != null:
		tile_to_destroy.queue_free()
	tile_to_destroy = observed_tile
	var new_tile = load(terrain_ref)
	var new_tile_pos = observed_tile.transform.origin
	new_tile_pos.z -= terrain_z_offset
	observed_tile = new_tile.instance()
	add_child(observed_tile)
	observed_tile.transform.origin = new_tile_pos
