extends Spatial

var node_ref: String = "res://AudioNodes/AudioNode.tscn"
var total_audioNodes: int = 0
export var max_audioNodes: int
var instantiation_position: Vector3

func _ready():
    instantiation_position = $Instantiation_position.global_transform.origin
    # The following only works if I bring back the Globals.gd from the 6/21/22 push
    # for list in Globals.revealed_item_lists:
    # 	max_audioNodes += list.size()
    $Instantiation_timer.start()

func _on_Instantiation_timer_timeout():
    instantiation_position = Globals.new_future_location()
    var new_scene = load(node_ref)
    var new_audioNode = new_scene.instance()
    add_child(new_audioNode)
    new_audioNode.global_transform.origin = instantiation_position
    new_audioNode.scale = Vector3(Globals.x_scale_default, \
            Globals.y_scale_default, Globals.z_scale_default)
    total_audioNodes += 1

func _on_node_grabbed():
    if total_audioNodes < max_audioNodes:
        $Instantiation_timer.start()
