extends Spatial
# Note: city is parented to terrain

export(Array, Texture) var shapes
var min_size = .5
var max_size = 2
export var randomize_shapes = true
# Choose which shape, if randomize_shapes is false
export var default_shape_index: int

func _ready():
	initialize()

func initialize():
	var my_sprite: Sprite3D = $Sprite3D
	my_sprite.scale.x = rand_range(min_size, max_size)
	my_sprite.scale.y = rand_range(min_size, max_size)
	my_sprite.flip_h = rand_range(0,1)
	if randomize_shapes:
		my_sprite.texture = shapes[rand_range(0, shapes.size())]
	else:
		my_sprite.texture = shapes[default_shape_index]

func _on_player_facing(var direction: String):
	match direction:
		"Front":
			if $AnimationPlayer.get_current_animation_position() > 0:
				$AnimationPlayer.play_backwards("duck")
		_:
			$AnimationPlayer.play("duck")
