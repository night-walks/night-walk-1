extends Spatial

var speed: float = 10.0
enum DIRECTION {FORWARD, BACKWARD, LEFT, RIGHT}
export var first_tile = false

func _ready():
    initialize()
    revelations()

func _process(delta):
	move_tiles(delta)

func initialize():
    choose_rotation()
	
func move_tiles(delta):
    if !$"../Timer_startMovement".is_stopped():
        return
    var next_stop = transform.origin
    next_stop.z += speed*delta
    transform.origin = next_stop
	
func choose_rotation():
    if first_tile: 
        return
    var facing : int = randi() % ( DIRECTION.RIGHT + 1 )
    print ("Now facing ", facing)
    match facing:
        DIRECTION.FORWARD:
            rotation_degrees = Vector3.ZERO
        DIRECTION.BACKWARD:
            rotation_degrees = Vector3(0, 180, 0)
        DIRECTION.LEFT:
            rotation_degrees = Vector3(0, 270, 0)
        DIRECTION.RIGHT:
            rotation_degrees = Vector3(0, 90, 0)
        _:
            print("New tile doesn't have a direction.") 
            
func revelations():
    var floater = $NeighborF
    if first_tile:
        floater.queue_free()   
        return
    # This is the denominator of a fraction
    var visibility_likelihood : int = 3 
    if randi() % visibility_likelihood > 0:
        floater.queue_free()