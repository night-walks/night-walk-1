extends Spatial

var url_retrieve_data: String = "redacted from public source"
var play_session_addition: String = ""
signal change_in_num_trees_on_server
export var total_sprite_frames: int = 1

func _ready(): 
    play_session_addition = "&play_session=" + String( Globals.play_session )
    # get_initial_server_data()
    # $Timer_check_server.start()

func get_initial_server_data():
    get_num_trees()

func get_num_trees():
    var error
    var req: String = url_retrieve_data + play_session_addition + "&type_requested=num_trees"
    #print(req)
    error = $HTTPReq_tree_get.request(req)
    if error != OK:
        push_error("HTTPReq_tree_get had an error.")

func _on_HTTPReq_tree_get_request_completed(_result, _response_code, _headers, body):
    var text = body.get_string_from_utf8()
    if text == null:
        print ("There is no game data on the server.")
        return
    var myresult: int = int(float(text))
    #print ("tree number request received: ", myresult)
    if myresult != Globals.num_trees:
        Globals.num_trees = myresult
        emit_signal("change_in_num_trees_on_server")

func randomize_tree():
    randomize()
    $sprite_sheet.frame = randi() % total_sprite_frames

func _on_change_in_num_trees_on_server():
    reveal_tree()

func _on_Timer_check_server_timeout():
    play_session_addition = "&play_session=" + String( Globals.play_session )
    get_num_trees()

func reveal_tree():
    $sprite_sheet/AnimationPlayer.play()    

func _on_player_facing(var which_way: String):
    if which_way == "Front":
        reveal_tree()