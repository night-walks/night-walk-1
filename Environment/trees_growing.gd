extends Node

enum PLAYING_ANIMATION { FIRST_TREE, SECOND_TREE, THIRD_TREE, FINISHED }    
var current_animation = PLAYING_ANIMATION.FIRST_TREE
var animations
var is_paused: bool = false

func _ready():
	animations = [
		$tree_holder1/AnimationPlayer,
		$tree_holder2/AnimationPlayer,
		$tree_holder3/AnimationPlayer
	]

func _process(_delta):
	check_current_animation()
	
func _on_player_facing(var direction: String):
	# print ("facing: ", direction)
	match direction:
		"Rear":
			play_animation()
		_:
			pause_animation()
 
func play_animation():
	match current_animation:
		PLAYING_ANIMATION.FIRST_TREE:
			animations[0].play("rise")
			print("resuming first")
		PLAYING_ANIMATION.SECOND_TREE:
			animations[1].play("rise")
			print("resuming second")
		PLAYING_ANIMATION.THIRD_TREE:
			animations[2].play("rise")
			print("resuming third")
		PLAYING_ANIMATION.FINISHED:
			pass
	is_paused = false

func pause_animation():
	if (is_paused):
		return
	match current_animation:
		PLAYING_ANIMATION.FIRST_TREE:
			animations[0].stop(false)
		PLAYING_ANIMATION.SECOND_TREE:
			animations[1].stop(false)
		PLAYING_ANIMATION.THIRD_TREE:
			animations[2].stop(false)
		PLAYING_ANIMATION.FINISHED:
			$Timer_tree.start()
	is_paused = true

func check_current_animation():
	if is_paused:
		return
	if animations[0].is_playing():
		current_animation = PLAYING_ANIMATION.FIRST_TREE
	elif animations[1].is_playing():
		current_animation = PLAYING_ANIMATION.SECOND_TREE
	elif animations[2].is_playing():
		current_animation = PLAYING_ANIMATION.THIRD_TREE
	else:
		current_animation = PLAYING_ANIMATION.FINISHED
	
func _on_timer_finished_reset_all():
	current_animation = PLAYING_ANIMATION.FIRST_TREE
	for anim in animations:
		anim.advance(0)
