extends KinematicBody

var speed: float = 1

# Flat variables
var mouse_sens = 0.3
var camera_anglev=0

# VR variables
var dir: Vector3

func _ready():
	if !Globals.isVR:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(_delta):
	get_input()
	var _noreturn = move_and_slide(dir)
	if Globals.isVR:
		print("player_move.gd's dir: ", dir)
		add_vignette()
	
func get_input():
    dir = Vector3.ZERO
    if Input.is_action_pressed("Move_forward"): # backward in VR
        if Globals.isVR:
            dir = $Player_VR/ARVRCamera.get_global_transform().basis.z.normalized()
            dir.y = 0
            dir *= speed
        else:
            dir = get_global_transform().basis.z.normalized() * -speed
    elif Input.is_action_pressed("Move_backward"): # forward in VR
        if Globals.isVR:
            dir = $Player_VR/ARVRCamera.get_global_transform().basis.z.normalized()
            dir.y = 0
            dir *= -speed
        else:
            dir = get_global_transform().basis.z.normalized() * speed
    if Input.is_action_pressed("Left_strafe"):
        if Globals.isVR:
            dir = $Player_VR/ARVRCamera.get_global_transform().basis.x.normalized()
            dir.y = 0
            dir *= -speed
        else:
            dir = get_global_transform().basis.x.normalized() * -speed
    elif Input.is_action_pressed("Right_strafe"):
        if Globals.isVR:
            dir = $Player_VR/ARVRCamera.get_global_transform().basis.x.normalized()
            dir.y = 0
            dir *= speed
        else:
            dir = get_global_transform().basis.x.normalized() * speed

func add_vignette():
	if dir == Vector3.ZERO:
		$Player_VR/ARVRCamera/Vignette.visible = false
	else:
		$Player_VR/ARVRCamera/Vignette.visible = true
	
# Flat
func _input(event):
	if Globals.isVR:
		return
	if event is InputEventMouseMotion:
		rotate_camera(event)

func rotate_camera(event):
	rotate_y(deg2rad(-event.relative.x*mouse_sens)) # relative to previous frame
	var changev=event.relative.y*mouse_sens
	if camera_anglev+changev>-50 and camera_anglev+changev<50:
		camera_anglev+=changev
		$Camera.rotate_x(deg2rad(-changev))
