extends Camera

var ray: RayCast
var current_direction: String

func _ready():
	if Globals.isVR == true:
		ray = get_node("/root/Main/PlayerController_XR/Player_VR/ARVRCamera/RayCast")
	else:
		ray = get_node("/root/Main/PlayerController_Flat/Camera/RayCast")

func _physics_process(_delta):
	if ray.is_colliding():
		var hitParent: String = ray.get_collider().get_parent().get_name()
		if current_direction != hitParent:
			current_direction = hitParent
			get_tree().call_group("neighbors", "_on_player_turned_cam", hitParent)
			get_tree().call_group("move_when_watched", "_on_player_facing", hitParent)