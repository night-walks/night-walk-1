extends ARVRController

export var hand_default_material: Material
export var hand_hovering_material: Material
# TODO These are used, right?
signal entered_invisible_body(body)
signal exited_invisible_body(body)
signal add_item_to_hand(item)
var original_parent
var body_current
var held_current
var grip_is_pressed: bool

func _ready():
	var hand_scale: float = $hand_model.scale.z
	if self.controller_id == 2:
		$hand_model.scale = Vector3(hand_scale, -hand_scale, hand_scale)
	visible = false
	if !is_connected("add_item_to_hand", self, "_on_add_item_to_hand"):
		if connect("add_item_to_hand", self, "_on_add_item_to_hand"):
			print ("Failed to connect add_item_to_hand.")
	if !is_connected("entered_invisible_body", self, "_on_entered_invisible_body"):
		if connect("entered_invisible_body", self, "_on_entered_invisible_body"):
			print ("Failed to connect entered_invisible_body.")
	if !is_connected("exited_invisible_body", self, "_on_exited_invisible_body"):
		if connect("exited_invisible_body", self, "_on_exited_invisible_body"):
			print ("Failed to connect exited_invisible_body.")

func _on_entered_invisible_body(body):
	if held_current != null:
		return
	body_current = body
	body_current.emit_signal("empty_hand_entered", self)
	rumble = .25

func _on_exited_invisible_body(body):
	body.emit_signal("empty_hand_exited", self)
	body_current = null
	rumble = 0

func _process(_delta):
	if !get_is_active():
		visible = false
	elif !visible:
		# make it visible
		visible = true
	if is_button_pressed(JOY_VR_GRIP):
		grip_close()
	else:
		grip_release()
	
func grip_close():
	if grip_is_pressed:
		return
	grip_is_pressed = true
	$hand_model/AnimationPlayer.play("ArmatureAction")
	if body_current != null:
		if !body_current.is_held:
			hold_item(body_current)
	
func grip_release():
	if !grip_is_pressed:
		return
	grip_is_pressed = false
	$hand_model/AnimationPlayer.play("ArmatureAction", -1, -1, true)
	$hand_model.visible = true
	if held_current != null:
		release_item()
	
func hold_item(var item):
	$hand_model.visible = false
	held_current = item
	item.is_held = true
	original_parent = item.get_parent()
	original_parent.remove_child(item)
	self.add_child(item)
	item.translation = Vector3(0,0,0)
	item.emit_signal("grip_begin")
	var streams = get_tree().get_root().find_node("Audio_shared", true, false).touch_invisible_objects

	if $AudioStreamPlayer3D.playing:
		return
	$AudioStreamPlayer3D.stream = streams[rand_range(0,streams.size())]
	$AudioStreamPlayer3D.play()
	
func release_item():
	held_current.is_held = false
	self.remove_child(held_current)
	original_parent.add_child(held_current)
	held_current.global_transform.origin = self.global_transform.origin
	held_current.emit_signal("grip_release")
	held_current = null

# Allows another node to add items to the hand
func _on_add_item_to_hand(var item):
	if held_current != null:
		release_item()
	hold_item(item)
