extends Spatial

export var hand_default_material: Material
export var hand_hovering_material: Material
# TODO these are used?
signal entered_invisible_body(body)
signal exited_invisible_body(body)
signal add_item_to_hand(item)
var original_parent
var body_current
var held_current
var grip_is_pressed: bool

func _ready():
	# These seem to be added, no? Just neutralize the error?
	if connect("add_item_to_hand", self, "_on_add_item_to_hand"):
		print ("Failed to connect add_item_to_hand.")
	if connect("entered_invisible_body", self, "_on_entered_invisible_body"):
		print ("Failed to connect entered_invisible_body.")
	if connect("exited_invisible_body", self, "_on_exited_invisible_body"):
		print ("Failed to connect exited_invisible_body.")

func _on_entered_invisible_body(body):
	if held_current != null:
		return
	$CPUParticles.visible = true
	body_current = body
	body_current.emit_signal("empty_hand_entered", self)

func _on_exited_invisible_body(body):
	$CPUParticles.visible = false
	body.emit_signal("empty_hand_exited", self)
	body_current = null

func _process(_delta):
	if Input.is_mouse_button_pressed(1):
		grip_close()
	else:
		grip_release()
	
func grip_close():
	if grip_is_pressed:
		return
	grip_is_pressed = true
	$hand_model/AnimationPlayer.play("ArmatureAction")
	if body_current != null:
		if !body_current.is_held:
			hold_item(body_current)
	
func grip_release():
	if !grip_is_pressed:
		return
	grip_is_pressed = false
	$hand_model/AnimationPlayer.play("ArmatureAction", -1, -1, true)
	$hand_model.visible = true
	if held_current != null:
		release_item()
	
func hold_item(var item):
	$hand_model.visible = false
	held_current = item
	item.is_held = true
	original_parent = item.get_parent()
	original_parent.remove_child(item)
	self.add_child(item)
	item.translation = Vector3(0,0,0)
	item.emit_signal("grip_begin")
	var streams = get_tree().get_root().find_node("Audio_shared", true, false).touch_invisible_objects

	if $AudioStreamPlayer3D.playing:
		return
	$AudioStreamPlayer3D.stream = streams[rand_range(0,streams.size())]
	$AudioStreamPlayer3D.play()
	
func release_item():
	held_current.is_held = false
	self.remove_child(held_current)
	original_parent.add_child(held_current)
	held_current.global_transform.origin = self.global_transform.origin
	held_current.emit_signal("grip_release")
	held_current = null

# Allows another node to add items to the hand
func _on_add_item_to_hand(var item):
	if held_current != null:
		release_item()
	hold_item(item)
