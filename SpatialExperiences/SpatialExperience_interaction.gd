extends Spatial

export var keep_visible: bool = false
export var linked_experience_location_path: String
var my_linked_location: Vector3

var isHeld: bool
# TODO Connect via script?
signal grip_begin
signal grip_release
signal empty_hand_entered
signal empty_hand_exited


func _ready():
	if !keep_visible:
		$MeshInstance.visible = false
	if get_parent().get_node(linked_experience_location_path) == null:
		print (name, " does not have a linked location")
	else:
		my_linked_location = get_parent().get_node(linked_experience_location_path).global_transform.origin

func _on_empty_hand_entered(body):
	if $AudioStreamPlayer3D.playing:
		return
	# TODO What was this attempting to do?
#	var streams = get_tree().get_root().find_node("Audio_shared", true, false).touch_invisible_objects
#	$AudioStreamPlayer3D.stream = streams[rand_range(0,streams.size())]
#	$AudioStreamPlayer3D.play()

func _on_empty_hand_exited(body):
	pass # Replace with function body.

func _on_AudioNode_grip_begin():
	Globals.player_last_location = Globals.player_cam.global_transform.origin
	Globals.player_cam.global_transform.origin = my_linked_location
	print("gripping")

func _on_AudioNode_grip_release():
	Globals.player_cam.global_transform.origin = Globals.player_last_location

func _on_Area_body_entered(body):
	body.get_parent().emit_signal("entered_invisible_body", self)

func _on_Area_body_exited(body):
	body.get_parent().emit_signal("exited_invisible_body", self)

func _on_Timer_timeout():
	pass
