extends Spatial

export var min_wait_time: int
export var max_wait_time: int

func _ready():
	initialize_timer()
	$AnimationPlayer.stop(true)

func initialize_timer():
	$Timer.wait_time = rand_range(min_wait_time, max_wait_time)
	$Timer.start()

func _on_Timer_timeout():
	$AnimationPlayer.play()
	initialize_timer()