extends Node

# Backlog of HTTP requests
var todo = Array()

func _ready():
    check_game_version_on_server()

func check_game_version_on_server():
    var error
    error = $HTTPRequest_CheckVersion.request(Globals.url_retrieve_data + "&thought_recalled=version")
    if error != OK:
        push_error("HTTPReq_version_check had an error.")
        
func _on_HTTPRequest_CheckVersion_request_completed(_result:int, _response_code:int, _headers:PoolStringArray, body:PoolByteArray):
    print("completed")
    var text = body.get_string_from_utf8()
    if text == null:
        print ("There is no game data on the server.")
        return
    var myresult: float = float(text)
    var local_version : float = ProjectSettings.get_setting("application/config/version")
    print("Local version:", local_version)
    if is_equal_approx(myresult, local_version):
        return
    if myresult < local_version:
        update_version_number_on_server(local_version)
    elif myresult > local_version:
        print("Server version: ", myresult)
        print("Local version:", local_version)
        print("This is an older version of the game. Disconnecting from server.")
        make_local_only()

func update_version_number_on_server(var version:float):
    var error
    error = $HTTPRequest_VersionUpdate.request(Globals.url_add_data + "&thought=version&value='" + String(version) + "'")
    if error != OK:
        push_error("HTTPRequest_VersionUpdate had an error.")

# Turns off future server queries if local version is too old
func make_local_only():
    Globals.connecting_to_server = false

func _on_check_server_timer_timeout():
    check_game_version_on_server()

func _on_player_facing(var direction: String):
    if direction == "Front":
        var current_game_time = OS.get_ticks_msec()
        upload_glance_data(current_game_time)
            
func upload_glance_data(var glance_time: int):
    var url = Globals.url_add_data + "&thought=Last_glance&quantity='" + String(glance_time) + "'"
    var error = $"HTTPRequest_SendGlance".request(url)
    if error == 44:
        todo.append(glance_time)
    elif error != OK:
        push_error("HTTP_Request_SendGlance had an error: " + String(error))

func _on_HTTPRequest_SendGlance_request_completed(_result, _response_code, _headers, _body):
    check_todo_list()
    
func check_todo_list():
    if todo.size() > 0:
        var next_glance_time = todo.pop_front()
        upload_glance_data(next_glance_time)
