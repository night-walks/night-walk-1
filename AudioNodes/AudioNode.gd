extends Spatial

# Firefly effects 
var waypoint_current: Vector3 = Vector3.ZERO
var min_waypoint_dist: float = 1
var move_speed_current: float = 0
var speed_original: float = 0
var speed_goal: float = 0
var move_speed_min: float = 1
var move_speed_max: float = 5
var changing_speed: bool = false
var speed_change_duration: float
var speed_change_elapsed_time: float
var speed_change_duration_min: float = 1.0 
var speed_change_duration_max: float = 3.0
var time_until_speed_change_min: float = 1.0
var time_until_speed_change_max: float = 3.0
var direction: Vector3
var color_transition_min_time: float = 5.0 
var color_transition_max_time: float = 15.0
var scale_original = Vector3( .165, .3, .05 )
var scale_current: Vector3 = scale_original
var scale_decrease_amt: float = 0.05

# Interaction
var is_held: bool
# TODO What to do with these to make it clear that they're called?
signal grip_begin
signal grip_release
signal empty_hand_entered
signal empty_hand_exited
signal node_grabbed
var original_location: Vector3
var is_already_released: bool = false
var is_firefly = true

# Revealed Item
var layer_in_NW2: int = 9 
var revealed_item_scale : float = 2.5 # old: 5
signal being_replaced

func _ready():
    # AudioNodes aren't really serving a function anymore besides being a flashing pixel
    # create_revealed_item()
    original_location = self.global_transform.origin
    $AudioStreamPlayer3D.max_db = get_tree().get_root().find_node("Audio_shared", true, false).audio_node_max_db
    $AudioStreamPlayer3D.unit_db = get_tree().get_root().find_node("Audio_shared", true, false).audio_node_db
    get_new_waypoint()
    set_scale( scale_original )
    set_color_change_timer()
    begin_speed_change()
    if connect("being_replaced", self, "_on_being_replaced"):
        print ("Get your signal names right, loser")
    if connect("node_grabbed", $"/root/Main/Future", "_on_node_grabbed"):
        print("Can't connect node_grabbed.")

func _input(event): 
    if event.is_action_pressed("Test_network_action"):
        update_server_items_moved()

func _process(delta):
    if !is_held && !is_already_released:
        if changing_speed:
            adjust_movement_speed(delta)
        movement(delta)

func movement(delta):
    if get_global_transform().origin.distance_squared_to(waypoint_current) \
            < min_waypoint_dist:
        get_new_waypoint()
    direction = ( waypoint_current - get_global_transform().origin ).normalized()
    translate( direction * delta * move_speed_current )
    if !is_firefly:
        # shrink every frame
        if scale_current > Vector3.ZERO:
            var decrease_amt_this_frame = Vector3( scale_decrease_amt, scale_decrease_amt,  0.0 ) * delta
            scale_current -= decrease_amt_this_frame
            if scale_current.x < 0.0:
                scale_current.x = 0.0
            if scale_current.y < 0.0:
                scale_current.y = 0.0
        set_scale( scale_current )

func get_new_waypoint():
    waypoint_current = Globals.new_future_location()
    # If returning from balcony
    if !is_firefly:
        revert_to_firefly()

func begin_speed_change():
    changing_speed = true
    speed_change_duration = rand_range(speed_change_duration_min, speed_change_duration_max)
    speed_change_elapsed_time = 0.0
    speed_goal = rand_range(move_speed_min, move_speed_max)
    speed_original = move_speed_current

func adjust_movement_speed(delta):
    var lerp_amt = speed_change_elapsed_time / speed_change_duration
    move_speed_current = lerp(speed_original, speed_goal, lerp_amt)
    if speed_change_elapsed_time >= speed_change_duration:
        $Speed_change_Timer.wait_time = rand_range(time_until_speed_change_min, time_until_speed_change_max)
        $Speed_change_Timer.start()
        changing_speed = false
    speed_change_elapsed_time += delta

func _on_Color_change_Timer_timeout():
    $Sprite3D.get_child(0).play("color_lerp")

func set_color_change_timer():
    var color_change_time = rand_range(color_transition_min_time, color_transition_max_time)
    $Color_change_Timer.wait_time = color_change_time
    $Color_change_Timer.start()

func create_revealed_item():
    randomize()
    # Chooses which layer, -4 through +4
    var random_list_num: int = randi() % Globals.revealed_item_lists.size()
    var random_list = Globals.revealed_item_lists[ random_list_num ]    
    # Chooses an item's reference string from that specific layer
    var random_item: String = random_list[ randi() % random_list.size() ]
    # Checks to see if exists, and if so, exits the function and restarts
    if check_revealed_item_already_exists(random_item):
        create_revealed_item()
        return
    # Loads the pre-existing scene into memory (prefab)
    var new_scene = load(random_item)
    if new_scene == null:
        print (random_item, ", which is ", random_list_num, " from ", random_list, " is a null scene")
    # Instantiates the scene
    var new_item = new_scene.instance()
    # Tells the new revealed item the path of its original object reference
    new_item.my_revealed_item_reference = random_item
    # tell revealed item which layer it is on 
    var layer_num: int = random_list_num - 4
    layer_in_NW2 = layer_num
    # Adds this item to the overall list of already-created items
    Globals.revealed_items_instantiated.append(new_item)
    # Adds it as a child to the audionode
    add_child(new_item)
    # Gives it default scale
    new_item.scale = Vector3(revealed_item_scale, revealed_item_scale, revealed_item_scale)

func check_revealed_item_already_exists(var item_name: String):
    var exists: bool = false 
    for existing_item in Globals.revealed_items_instantiated:
        if existing_item.my_revealed_item_reference == item_name:
            exists = true
    return exists
    
func _on_empty_hand_entered(_body):
    if get_node("Revealed_Item") != null:
        get_node("Revealed_Item").emit_signal("is_touched")
    
func _on_empty_hand_exited(_body):
    if get_node("Revealed_Item") != null:
        get_node("Revealed_Item").emit_signal("is_untouched")

func _on_AudioNode_grip_begin():
    if get_node("Revealed_Item") == null:
        return
    if !$Revealed_Item.visible:
        $Revealed_Item.visible = true
        # Turns off this collider, to be replaced by the Revealed_Item's collider
        $Area.disconnect("body_entered", self, "_on_Area_body_entered")
        $Area.disconnect("body_exited", self, "_on_Area_body_exited")
        $Revealed_Item.emit_signal("make_new_connections")
        # Makes firefly dot invisible
        $Sprite3D.visible = false
        is_firefly = false
        # Replaces the previous item from the same layer, if necessary
        replace_revealed_item()
        emit_signal("node_grabbed")

func _on_AudioNode_grip_release():
    if !is_already_released:
        is_already_released = true
        update_server_item_num()
    else:
        update_server_items_moved()

func _on_Area_body_entered(body):
    body.get_parent().emit_signal("entered_invisible_body", self)
    if !$Revealed_Item.visible:
        $Sprite3D.modulate.a = 1 

func _on_Area_body_exited(body):
    body.get_parent().emit_signal("exited_invisible_body", self)
    if !$Revealed_Item.visible:
        $Sprite3D.modulate.a = .47

func replace_revealed_item():
    # TODO Find out if global variable for this object is already filled 
        # if so, send a signal to that object 
        # fill the global variable for that layer with this object with the current AudioNode
    match layer_in_NW2:
        -4:
            if Globals.rev_neg_4_instantiated != null:
                Globals.rev_neg_4_instantiated.emit_signal("being_replaced")
            Globals.rev_neg_4_instantiated = self 
            Globals.times_neg_4_found += 1 
            update_times_found_per_layer( "times_neg_4_found", Globals.times_neg_4_found )
        -3:
            if Globals.rev_neg_3_instantiated != null:
                Globals.rev_neg_3_instantiated.emit_signal("being_replaced")
            Globals.rev_neg_3_instantiated = self 
            Globals.times_neg_3_found += 1 
            update_times_found_per_layer( "times_neg_3_found", Globals.times_neg_3_found )
        -2: 
            if Globals.rev_neg_2_instantiated != null:
                Globals.rev_neg_2_instantiated.emit_signal("being_replaced")
            Globals.rev_neg_2_instantiated = self
            Globals.times_neg_2_found += 1 
            update_times_found_per_layer( "times_neg_2_found", Globals.times_neg_2_found )
        -1:
            if Globals.rev_neg_1_instantiated != null:
                Globals.rev_neg_1_instantiated.emit_signal("being_replaced")
            Globals.rev_neg_1_instantiated = self 
            Globals.times_neg_1_found += 1 
            update_times_found_per_layer( "times_neg_1_found", Globals.times_neg_1_found )
        0: 
            if Globals.rev_zero_instantiated != null:
                Globals.rev_zero_instantiated.emit_signal("being_replaced")
            Globals.rev_zero_instantiated = self 
            Globals.times_zero_found += 1 
            update_times_found_per_layer( "times_zero_found", Globals.times_zero_found )
        1: 
            if Globals.rev_pos_1_instantiated != null:
                Globals.rev_pos_1_instantiated.emit_signal("being_replaced")
            Globals.rev_pos_1_instantiated = self 
            Globals.times_pos_1_found += 1 
            update_times_found_per_layer( "times_pos_1_found", Globals.times_pos_1_found )
        2: 
            if Globals.rev_pos_2_instantiated != null:
                Globals.rev_pos_2_instantiated.emit_signal("being_replaced")
            Globals.rev_pos_2_instantiated = self 
            Globals.times_pos_2_found += 1 
            update_times_found_per_layer( "times_pos_2_found", Globals.times_pos_2_found )
        3: 
            if Globals.rev_pos_3_instantiated != null:
                Globals.rev_pos_3_instantiated.emit_signal("being_replaced")
            Globals.rev_pos_3_instantiated = self 
            Globals.times_pos_3_found += 1 
            update_times_found_per_layer( "times_pos_3_found", Globals.times_pos_3_found )
        4:
            if Globals.rev_pos_4_instantiated != null:
                Globals.rev_pos_4_instantiated.emit_signal("being_replaced")
            Globals.rev_pos_4_instantiated = self 
            Globals.times_pos_4_found += 1 
            update_times_found_per_layer( "times_pos_4_found", Globals.times_pos_4_found )

func update_times_found_per_layer( variable_name, value ):
    var gameURL = "redacted from public source"
    var value_string = String( value )
    server_request_add_data( variable_name, value_string, gameURL )

func update_server_item_num():
    Globals.num_objects_found += 1
    var gameURL = "redacted from public source"
    var var_name = "num_objects_found"
    var value = String(Globals.num_objects_found)
    server_request_add_data(var_name, value, gameURL)
    
func update_server_items_moved():
    Globals.num_objects_moved += 1
    var gameURL = "redacted from public source"
    var var_name = "num_objects_moved"
    var value = String(Globals.num_objects_moved)
    server_request_add_data(var_name, value, gameURL)
    
func server_request_add_data(var var_name, var value, var gameURL):
    var url = gameURL + "?action=add_data" \
        + "&play_session=" + str( Globals.play_session ) \
        + "&var_name=" + var_name \
        + "&value=" + value
    $HTTPRequest.request(url)
    
# called by a signal that is, itself, called by the AudioNode that replaced me
func _on_being_replaced():
    # return to landscape area
    is_already_released = false
    
# happens the moment reaches first waypoint in reversion process
func revert_to_firefly():
    $Revealed_Item.visible = false 
    # turn AudioNode's collider back on 
    if !$Area.connect("body_entered", self, "_on_Area_body_entered"):
        print ("Failed to connect body_entered.")
    if !$Area.connect("body_exited", self, "_on_Area_body_exited"):
        print ("Failed to connect body_exited.")
    # turn Revealed_Item's collider off 
    $Revealed_Item.emit_signal("remove_connections")
    # revert to firefly visual
    $Sprite3D.visible = true
    # stop reversion from happening again
    is_firefly = true
    # revert AudioNode to original size
    set_scale( scale_original )
