extends Sprite3D

var playerCamFlat : Camera
var playerCamARVR : ARVRCamera
var camLoc : Vector3
var XR: bool
var my_revealed_item_reference: String
signal is_touched
signal is_untouched
signal make_new_connections
signal remove_connections

func _ready():
	self.visible = false
	if Globals.isVR:
		playerCamARVR = get_tree().get_root().find_node("ARVRCamera", true, false)
		XR = true
	else:
		playerCamFlat = get_tree().get_root().find_node("Camera", true, false)
		XR = false
	$AnimationPlayer.stop()
		
	implement_standard_values()
	if !is_connected("is_touched", self, "_on_Revealed_Item_is_touched"):
		if connect("is_touched", self, "_on_Revealed_Item_is_touched"):
			print ("Can't connect is_touched.")
	if !is_connected("is_untouched", self, "_on_Revealed_Item_is_untouched"):
		if connect("is_untouched", self, "_on_Revealed_Item_is_untouched"):
			print ("Can't connect is_untouched.")
	if !is_connected("make_new_connections", self, "_on_make_new_connections"):
		if connect("make_new_connections", self, "_on_make_new_connections"):
			print("Can't connect make_new_connections.")
	if !is_connected("remove_connections", self, "_on_remove_connections"):
		if connect("remove_connections", self, "_on_remove_connections"):
			print("Can't connect remove_connections.")

	
	
func _process(_delta):
	if !self.visible:
		return
	if Globals.isVR:
		look_at(playerCamARVR.global_transform.origin, Vector3.UP)
	else:
		look_at(playerCamFlat.global_transform.origin, Vector3.UP)
	
func implement_standard_values():
	modulate = Color(0,0,0)
	billboard = SpatialMaterial.BILLBOARD_DISABLED
	alpha_cut = SpriteBase3D.ALPHA_CUT_OPAQUE_PREPASS


func _on_Revealed_Item_is_touched():
	modulate = Color(.1,.1,.1)

func _on_Revealed_Item_is_untouched():
	modulate = Color(0,0,0)

func _on_make_new_connections():
	if !$Area.connect("body_entered", self.get_parent(), "_on_Area_body_entered"):
		print ("Failed to connect body_entered.")
	if !$Area.connect("body_exited", self.get_parent(), "_on_Area_body_exited"):
		print ("Failed to connect body_exited.")

func _on_remove_connections():
	$Area.disconnect("body_entered", self.get_parent(), "_on_Area_body_entered")
	$Area.disconnect("body_exited", self.get_parent(), "_on_Area_body_exited")

