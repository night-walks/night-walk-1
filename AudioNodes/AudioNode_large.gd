extends Spatial

var isHeld: bool
# TODO figure out
signal grip_begin
signal grip_release
signal empty_hand_entered
signal empty_hand_exited
var current_hand: ARVRController
var revealed_item_scale : float = 10
var original_location: Vector3

var items = [
	"res://RevealedItems/Item_Berries.tscn",
	"res://RevealedItems/Item_Bird.tscn",
	"res://RevealedItems/Item_Cloud.tscn",
	"res://RevealedItems/Item_Cow.tscn",
	"res://RevealedItems/Item_LookStand.tscn",
	"res://RevealedItems/Item_StatueMourning.tscn"
]

func _ready():
	$MeshInstance.visible = false
#	create_revealed_item()
	original_location = self.global_transform.origin

func create_revealed_item():
	var random_item: int = int(rand_range(0, items.size()))
	var new_scene = load(items[random_item])
	var new_item: Spatial = new_scene.instance()
	new_item.visible = true
	get_node("/root/Main/RevealedItems/").add_child(new_item)
	new_item.scale = Vector3(revealed_item_scale, revealed_item_scale, revealed_item_scale)
	current_hand.emit_signal("_on_add_item_to_hand", new_item)

func _on_empty_hand_entered(body):
	current_hand = body
	
func _on_empty_hand_exited(_body):
	current_hand = null

func _on_AudioNode_grip_begin():
	create_revealed_item()

func _on_AudioNode_grip_release():
	self.global_transform.origin = original_location

func _on_Area_body_entered(body):
	body.get_parent().emit_signal("entered_invisible_body", self)

func _on_Area_body_exited(body):
	body.get_parent().emit_signal("exited_invisible_body", self)
