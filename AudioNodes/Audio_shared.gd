extends Node

# Flyweight for invisible audio objects

export(Array, AudioStream) var touch_invisible_objects
export (Array, AudioStream) var hold_sounds
export var audio_node_db: float
export var audio_node_max_db: float
