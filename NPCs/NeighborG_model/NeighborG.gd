extends Spatial

var has_played: bool = false

func _on_player_facing(var direction: String):
    if (direction == "Rear" && !has_played):
        $AnimationPlayer.play("Falling")
        $AnimationPlayer_exit.play("FallMore")
        has_played = true

func _on_RestartTimer_timeout():
    has_played = false
