extends KinematicBody

# Movement variables
var waypoint_pos = Vector3()
var speed: float
var dir = Vector3() 
var distance: float

# Movement constants
const UP = Vector3.UP
const MAX_SPEED = 5
const ACCEL = 1
const ARRIVE_THRESHOLD: float = 10.0
const X_MAX: float = 95.0
const X_MIN: float = -95.0 
const Z_MAX: float = .5
const Z_MIN: float = -95.0 

func _ready():
	get_random_position()

func _process(delta):
	move(delta)

# TODO Following waypoints doesn't quite work yet; doesn't appear to be turning to face or go in the right direction
func move(delta):
	look_at(waypoint_pos, UP)
	if (speed < MAX_SPEED): 
		speed += ACCEL * delta
	dir = -self.get_global_transform().basis.z.normalized()
	dir.y = 0
	dir *= speed
	dir = move_and_slide(dir)
	distance = (transform.origin - waypoint_pos).length()
	# print ("Position: " + String(transform.origin) + ", Rotation: " + String(transform.basis.get_euler()))
	if distance < ARRIVE_THRESHOLD:
		print ("Arrived at waypoint.")
		get_random_position()


func get_position_from_server():
	# TODO problem might be because it's trying to connect it twice
	var err = $HTTPRequest_get_x.connect("request_completed", self, "_on_get_x_position_request_completed")
	if err:
		print ("Failed to connect HTTPRequest_get_x," + String(err))
	if $HTTPRequest_get_y.connect("request_completed", self, "_on_get_y_position_request_completed"):
		print ("Failed to connect HTTPRequest_get_y.")
	$HTTPRequest_get_x.request(Globals.base_url + "action=retrieve_data&play_session=" +
			str(Globals.play_session) + "&type_requested=x_pos")
	$HTTPRequest_get_y.request(Globals.base_url + "action=retrieve_data&play_session=" +
			str(Globals.play_session) + "&type_requested=y_pos")

func _on_get_x_position_request_completed(_result, _response_code, _headers, body):
	var text = body.get_string_from_utf8()
	if text == null:
		print ("No x position found on server")
		return
	waypoint_pos.x = int(float(text))
# TODO change all this to reference z when fixed in NW5
func _on_get_y_position_request_completed(_result, _response_code, _headers, body):
	var text = body.get_string_from_utf8()
	if text == null:
		print ("No y position found on server")
		return
	# Convert y from NW5 to z
	waypoint_pos.z = int(float(text))
	print ("Position of wanderer's next waypoint: " + str(waypoint_pos.x) + ", " + str(waypoint_pos.z) )

func _on_Timer_timeout():
	# TODO revert back to getting position from server
	# get_position_from_server()
	# get_random_position()
	print("Distance: ", distance)
	$Timer.start()

func get_random_position():
	waypoint_pos = Vector3(rand_range(X_MIN, X_MAX), 18, rand_range(Z_MIN, Z_MIN))