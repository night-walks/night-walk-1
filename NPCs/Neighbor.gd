extends Spatial

enum Location {left, right}

export(Location) var myLoc
export(Array, String) var animation_names
export var default_time: float
export var min_wait: float = 2.0
export var max_wait: float = 10.0
export var static_location = false
export var ping_pong_loop_animations = true

var currently_facing: bool = false
var num_anim: int
var animation_locations = Array()

func _ready():
	animation_names = $AnimationPlayer.get_animation_list()
	num_anim = animation_names.size()
	if num_anim == 0:
		return
	animation_locations = self.get_parent().get_node("Animation_locations").get_children()
	change_animation()
	
func _process(_delta):
	if ping_pong_loop_animations:
		ping_pong_loop()
	
func ping_pong_loop():
	if !$AnimationPlayer.is_playing():
		if $AnimationPlayer.current_animation_position == 0:
			$AnimationPlayer.play()
		else:
			$AnimationPlayer.play_backwards()

func _on_player_turned_cam(dir: String):
	if myLoc == Location.left && dir == "Left":
		currently_facing = true;
	elif myLoc == Location.right && dir == "Right":
		currently_facing = true;
	else:
		if currently_facing:
			change_animation()
			pass
		currently_facing = false;

func change_animation():
	if num_anim==0:
		return
	randomize()
	var which_anim: int = int(rand_range(0,num_anim))
	var anim_name: String = animation_names[which_anim]
	if anim_name == "all_anim":
		change_animation()
		return
	$AnimationPlayer.play(anim_name)
	if !static_location:
		adjust_transform_to_animation(anim_name)

func adjust_transform_to_animation(animation_name: String):
	if animation_locations.size() == 0:
		return
	for loc in animation_locations:
		if loc.get_name() == animation_name:
			move_to_new_spatial_node(loc)
			return
	
func move_to_new_spatial_node(new_trans: Spatial):
	self.global_transform.origin = new_trans.global_transform.origin
	self.global_transform.basis = new_trans.global_transform.basis.get_euler()
	self.scale = new_trans.scale
	
