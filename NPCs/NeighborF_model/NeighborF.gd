extends Spatial

func _ready():
    jump_to_random_frame()
    
func jump_to_random_frame():
    var seek_to: float = rand_range(0, $AnimationPlayer.current_animation_length)
    $AnimationPlayer.seek(seek_to, true)