extends Spatial

export var default_time: float
export var min_wait: float = 2.0
export var max_wait: float = 10.0

func reverse_animation_direction():
	if $AnimationPlayer.current_animation_position == 0:
		$AnimationPlayer.playback_speed = 1.0
		print("playing forward")
	else:
		$AnimationPlayer.playback_speed = -1.0
		$AnimationPlayer.play()
		print ("reversing")
		
func jump_to_default_time():
	$AnimationPlayer.seek(default_time, true)

func pause_on_frame():
	$AnimationPlayer.stop(false)
	$Timer.wait_time = rand_range(min_wait, max_wait)
	print ("pausing for ", $Timer.wait_time, " on ", $AnimationPlayer.current_animation_position)
	$Timer.start()

func _on_Timer_timeout():
	$AnimationPlayer.play()
