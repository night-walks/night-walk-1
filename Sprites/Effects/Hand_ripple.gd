extends Sprite3D

export var hand_path: String
#var proximity_buffer: float = 0.3
var visible_distance: float = .3
var hand: ARVRController
var stable_Z_pos: float

func _ready():
	var system = OS.get_name()
	# Need better question to detect VR
	if system != "Android":
		self.queue_free()
	hand = get_node("/root/Main/"+hand_path)
	stable_Z_pos = self.global_transform.origin.z

func _process(_delta):
	follow_hand()
	change_visibility()

func follow_hand():
	var new_loc:Vector3 = hand.global_transform.origin
	new_loc.z = stable_Z_pos
	global_transform.origin = new_loc
	
func change_visibility():
	#print("Ripple Z: ", global_transform.origin.z, ", Hand Z: ", hand.global_transform.origin.z)
	#if hand.global_transform.origin.z < (global_transform.origin.z + proximity_buffer):
	
	var current_distance = global_transform.origin.distance_to(hand.global_transform.origin)
	#print ("Distance: ", current_distance)
	if current_distance < visible_distance:
		var alpha_distance = current_distance / visible_distance
		alpha_distance = 1-alpha_distance
		modulate = Color(0,0,0,alpha_distance)
		visible =true
	else:
		visible = false
