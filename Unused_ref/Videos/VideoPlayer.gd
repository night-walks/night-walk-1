extends VideoPlayer

export var loop: bool = true

func _process(delta):
	if loop:
		if !is_playing():
			play()
