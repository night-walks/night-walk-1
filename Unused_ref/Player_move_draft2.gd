extends KinematicBody

# Member variables
var g = -9.8
var vel = Vector3()
const MAX_SPEED = 5
const JUMP_SPEED = 7
const ACCEL= 2
const DEACCEL= 4
const MAX_SLOPE_ANGLE = 30


func _fixed_process(delta):
	var dir = Vector3() # Where does the player intend to walk to
	var cam_xform = get_node("target/camera").get_global_transform()
	
	if (Input.is_action_pressed("Move_forward")):
		dir += -cam_xform.basis[2]
	if (Input.is_action_pressed("Move_backwards")):
		dir += cam_xform.basis[2]
	if (Input.is_action_pressed("Left_strafe")):
		dir += -cam_xform.basis[0]
	if (Input.is_action_pressed("Right_strafe")):
		dir += cam_xform.basis[0]
	
	dir.y = 0
	dir = dir.normalized()
	
	vel.y += delta*g
	
	var hvel = vel
	hvel.y = 0
	
	var target = dir*MAX_SPEED
	var accel
	if (dir.dot(hvel) > 0):
		accel = ACCEL
	else:
		accel = DEACCEL
	
	hvel = hvel.linear_interpolate(target, accel*delta)
	
	vel.x = hvel.x
	vel.z = hvel.z
	
	var motion = move_and_collide(vel*delta)
	
