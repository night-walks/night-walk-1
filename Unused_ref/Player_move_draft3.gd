extends KinematicBody

# Some code borrowed from Food Fight tutorial by Ben Tristem, https://www.udemy.com/course/godot/learn/lecture/12785487


# movement variables
var vel = Vector3()
var dir = Vector3()
var facing_direction = 0

# movement constants
const UP = Vector3(0,1,0)
const MAX_SPEED: float = 5.0
const ACCEL: float = 2.5
const DECCEL: float = 5.0

# Flat & VR
var camera_xform: Transform

# Flat
var mouse_sens = 0.3
var camera_anglev=0

func _ready():
	# VR
	if $Player_VR/ARVRCamera != null:
		camera_xform = $Player_VR/ARVRCamera.get_global_transform()
		print ("Found VR camera")
	# Flat
	elif $Camera != null:
		camera_xform = get_global_transform()
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		print ("No camera found")

func _physics_process(delta):
	update_movement(delta)

func update_movement(delta):
	var movement_dir: Vector2 = get_2d_movement()
	
	dir = Vector3.ZERO
	
	print("basis: ", camera_xform.basis[2])
	if Input.is_action_pressed("Move_forward"):
		dir += -camera_xform.basis[2]
	elif Input.is_action_pressed("Move_backward"):
		dir += camera_xform.basis[2]
	if Input.is_action_pressed("Left_strafe"):
		dir += -camera_xform.basis[0]
	elif Input.is_action_pressed("Right_strafe"):
		dir += camera_xform.basis[0]
		
	dir.y = 0
	dir = dir.normalized()
		
	var hvel = vel
	hvel.y = 0
	
	var target = dir*MAX_SPEED
	var accel
	if (dir.dot(hvel) > 0):
		accel = ACCEL
	else:
		accel = DECCEL
		
	hvel = hvel.linear_interpolate(target, accel*delta)
	
	vel.x = hvel.x
	vel.z = hvel.z
	
	print(vel)
	
	move_and_slide(vel, UP)


	
func get_2d_movement():
	var movement_vector = Vector2()
	
	
		
	return movement_vector.normalized()
	
# VR button rotation - not used currently
#func get_chunky_rotation(delta):
#	if Input.is_action_pressed("Left_chunky"):
		# isn't working, don't necessarily need right now
#	elif Input.is_action_pressed("Right_chunky"):
		# isn't working, don't necessarily need right now
	
# Flat
func _input(event):
	if event is InputEventMouseMotion:
		rotate_camera(event)

func rotate_camera(event):
	rotate_y(deg2rad(-event.relative.x*mouse_sens)) # relative to previous frame
	var changev=event.relative.y*mouse_sens
	if camera_anglev+changev>-50 and camera_anglev+changev<50:
		camera_anglev+=changev
		$Camera.rotate_x(deg2rad(-changev))
