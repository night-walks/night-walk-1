extends KinematicBody

# Some code borrowed from Food Fight tutorial by Ben Tristem, https://www.udemy.com/course/godot/learn/lecture/12785487


# movement variables
var vel = Vector3()
var dir = Vector3()
var facing_direction = 0

# movement constants
const UP = Vector3(0,1,0)
const MAX_SPEED: float = 5.0
const ACCEL: float = 2.5
const DECCEL: float = 5.0

# Flat & VR
var camera_xform: Transform

# Flat
var mouse_sens = 0.3
var camera_anglev=0

func _ready():
	# VR
	if $Player_VR/ARVRCamera != null:
		camera_xform = $Player_VR/ARVRCamera.get_global_transform()
		print ("Found VR camera")
	# Flat
	elif $Camera != null:
		camera_xform = $Camera.get_global_transform()
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		print ("No camera found")

func _physics_process(delta):
	update_movement(delta)

func update_movement(delta):
	var movement_dir: Vector2 = get_2d_movement()
	
	dir = Vector3.ZERO
	
	dir += rotation_degrees * movement_dir.y
	dir += rotation_degrees * movement_dir.x
	print(dir)
#	dir += camera_xform.basis.z.normalized() * movement_dir.y
#	dir += camera_xform.basis.x.normalized() * movement_dir.x
	
	vel = h_accel(dir, delta)
	
	move_and_slide(vel, UP)

func h_accel(dir, delta):
	var vel_2D = vel
	vel_2D.y = 0
	
	var target = dir
	target *= MAX_SPEED
	
	var accel
	if dir.dot(vel_2D) > 0:
		accel = ACCEL
	else:
		accel = DECCEL
	
	vel_2D = vel_2D.linear_interpolate(target, accel * delta)
	
	vel.x = vel_2D.x 
	vel.z = vel_2D.z
	
	return vel
	
func get_2d_movement():
	var movement_vector = Vector2()
	
	if Input.is_action_pressed("Move_forward"):
		movement_vector.y = -1
	elif Input.is_action_pressed("Move_backward"):
		movement_vector.y = 1
	if Input.is_action_pressed("Left_strafe"):
		movement_vector.x = -1
	elif Input.is_action_pressed("Right_strafe"):
		movement_vector.x = 1
		
	return movement_vector.normalized()
	
# VR button rotation - not used currently
func get_chunky_rotation(delta):
	if Input.is_action_pressed("Left_chunky"):
		print("Left_chunky")
		# isn't working, don't necessarily need right now
	elif Input.is_action_pressed("Right_chunky"):
		print("Right_chunky")
		# isn't working, don't necessarily need right now
	
# Flat
func _input(event):
	if event is InputEventMouseMotion:
		rotate_camera(event)

func rotate_camera(event):
	rotate_y(deg2rad(-event.relative.x*mouse_sens)) # relative to previous frame
	var changev=event.relative.y*mouse_sens
	if camera_anglev+changev>-50 and camera_anglev+changev<50:
		camera_anglev+=changev
		$Camera.rotate_x(deg2rad(-changev))
