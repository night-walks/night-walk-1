extends Spatial

# VR
var ovr_init_config
var ovr_performance
var perform_runtime_config = false

func _ready():
    #detect_OS()
    if Globals.isVR:
        load_VR()
    else:
        load_flat()
    
func load_VR():
    $PlayerController_Flat.queue_free()
    Globals.playerController = $PlayerController_XR
    Globals.player_cam = $PlayerController_XR/Player_VR/
    if OS.get_name() == "Android":
        ovr_init_config = preload("res://addons/godot_ovrmobile/OvrInitConfig.gdns").new()
        ovr_performance = preload("res://addons/godot_ovrmobile/OvrPerformance.gdns").new()
        var interface = ARVRServer.find_interface("OVRMobile")
        if interface:
            ovr_init_config.set_render_target_size_multiplier(1)
            if interface.initialize():
                get_viewport().arvr = true
    else:
        print("Preparing for Vive")
        ovr_init_config = preload("res://addons/godot-openvr/OpenVRConfig.gdns").new()
        # var interface = ARVRServer.find_interface("OpenVR")
        get_viewport().arvr = true 
        get_viewport().hdr = false 
        OS.vsync_enabled = false 
        Engine.target_fps = 90


func load_flat():
    if !OS.has_feature("editor"):
        halt_play_non_VR()
    $PlayerController_XR.queue_free()
    Globals.playerController = $PlayerController_Flat
    Globals.player_cam = $PlayerController_Flat/Camera

func _process(_delta):
    main_inputs()
    if Globals.isVR && OS.get_name() == "Android":
        run_VR()

func main_inputs():
    if Input.is_action_just_pressed("Fullscreen"):
        OS.window_fullscreen = !OS.window_fullscreen
    if Input.is_action_just_pressed("Quit"):
        get_tree().quit()

func run_VR():
    if !perform_runtime_config:
        ovr_performance.set_clock_levels(1, 1)
        ovr_performance.set_extra_latency_mode(1)
        perform_runtime_config = true
    
func halt_play_non_VR():
    if get_tree().change_scene("res://NoPlay.tscn"):
        print("NoPlay scene not found.")