extends Node

var playerController: KinematicBody
var player_last_location: Vector3
var player_cam
var isVR: bool

# Server Data

var connecting_to_server : bool = true
# URL variables deleted in public code.

# Future boundaries

var x_min_bound: float = -2.5
var x_max_bound: float = 2.5
var y_min_bound: float = -.5
var y_max_bound: float = 4
var z_min_bound: float = -4
var z_max_bound: float = -2
var x_scale_default: float = .181 #.165
var y_scale_default: float = .181
var z_scale_default: float = .181 #.05

func new_future_location():
    randomize()
    var new_loc = Vector3()
    new_loc.x = rand_range( Globals.x_min_bound , Globals.x_max_bound )
    new_loc.y = rand_range( Globals.y_min_bound , Globals.y_max_bound )
    new_loc.z = rand_range( Globals.z_min_bound , Globals.z_max_bound )
    return new_loc