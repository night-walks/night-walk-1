extends Node

var current_object

func _ready():
	test()

func test():
	pick_json_object()
	print (current_object.phrase % current_object.numbers)
	print ("I am dead: ", current_object.dead)
	if current_object.food != null:
		print ("I am eating a ", current_object.food)
	print("Testing nested object: ", current_object.weapons["1"])

func pick_json_object():
	var objects = get_from_json("res://JSON/test.json")
	randomize() # shuffles random results
	current_object = objects[randi() % objects.size()]

func get_from_json(filename):
	var file = File.new()
	file.open(filename, File.READ)
	var text = file.get_as_text()
	var data = parse_json(text)
	file.close()
	return data
