extends Spatial

export var is_vr_desktop: bool = true

func _ready():
	# create_new_game_on_server()
	detect_OS() # skipping server stuff

func create_new_game_on_server():
	if $HTTPRequest_create_session.connect("request_completed", self,
		"_on_create_new_game_on_server_request_completed"):
		print("HTTPRequest_create_session failed to connect.")
	$HTTPRequest_create_session.request(
		Globals.base_url + "action=new_game")
	
func _on_create_new_game_on_server_request_completed(_result, _response_code, _headers, body):
	var text = body.get_string_from_utf8()
	if text == null:
		print ("No play sessions found on the server.")
		Globals.play_session = 0
		return
	Globals.play_session = int(float(text))
	print ("Play session: " , Globals.play_session)

	initialize_server_variables()

func initialize_server_variables():
	# number of items found
	if $HTTPRequest_item_num.connect("request_completed", self, "_on_item_num_request_completed"):
		print("HTTPRequest_item_num failed to connect.")
	var request_url = Globals.base_url + "action=add_data&play_session=" + str(Globals.play_session) + "&var_name=num_objects_found"  + "&value=0";
	$HTTPRequest_item_num.request(request_url)
	# number of items moved
	if $HTTPRequest_move_num.connect("request_completed", self, "_on_move_num_request_completed"):
		print("HTTPRequest_move_num failed to connect.")
	request_url = Globals.base_url + "action=add_data&play_session=" + str(Globals.play_session) + "&var_name=num_objects_moved"  + "&value=0";
	$HTTPRequest_move_num.request(request_url)


func _on_move_num_request_completed(_result, _response_code, _headers, _body):
	detect_OS()

# Called after final http request completed
func detect_OS():
	print ("Detecting OS...")
	var system = OS.get_name()
	print ("My OS: " + system)
	match system:
		"Android":
			Globals.isVR = true
			_on_Android()
		"HTML5":
			_on_HTML5()
		"OSX":
			_on_OSX()
		"X11":
			_on_Linux()
		"Windows":
			_on_Windows()
	if is_vr_desktop:
		Globals.isVR = true

func _on_Android():
	print("Confirming debug over USB")
	load_environment()

func _on_Windows():
	detect_VR()
	load_environment()
	
func _on_OSX():
	detect_VR()
	load_environment()
	
func _on_Linux():
	detect_VR()
	load_environment()
	
func _on_HTML5():
	load_environment()
	
func detect_VR():
	var VR = ARVRServer.find_interface("OpenVR")
	if VR && VR.initialize():
		Globals.isVR = true

func load_environment():
	if get_tree().change_scene("res://Environment/Environment.tscn"):
		print("Environment scene not found.")